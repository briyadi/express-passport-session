const router = require("express").Router()
const auth = require("./controllers/authController")
const user = require("./controllers/userController")
const authenticator = require("./lib/authenticator")

// .. definisi router
router.get("/login", auth.login)
router.post("/login", auth.loginPost)
router.get("/profile", authenticator, user.profile)
router.get("/logout", auth.logout)
router.use((err, req, res, next) => {
    if (err) {
        return res.send(err.message)
    }

    next()
})

module.exports = router