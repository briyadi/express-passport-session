const passport = require("passport")
const { Strategy: LocalStrategy } = require("passport-local")

const userData = {
    username: "sabrina@binar.com",
    fullname: "Administrator",
}

const validator = (username, password, done) => {
    if (username == "sabrina@binar.com" && password == "12345") {
        return done(null, userData)
    }

    done(new Error("invalid user"), false)
}

const strategy = new LocalStrategy({
    usernameField: "username",
    passwordField: "password"
}, validator)

passport.use(strategy)

passport.serializeUser((user, done) => done(null, user.username))
passport.deserializeUser(async (username, done) => {
    if (username == "sabrina@binar.com") {
        return done(null, userData)
    }

    done(new Error("unknown user"), false)
})

module.exports = passport