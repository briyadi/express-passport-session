module.exports = {
    profile: (req, res) => {
        let params = {
            username: req.user?.username
        }

        res.render("profile", params)
    }
}