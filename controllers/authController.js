const passport = require("../lib/passport")

module.exports = {
    login: (req, res) => {
        res.render("login")
    },
    loginPost: passport.authenticate("local", {
        successRedirect: "/profile",
        failureRedirect: "/login",
        failureFlash: true,
    }),
    logout: (req, res, next) => {
        req.logout(err => {
            if (err) return next()
            res.redirect("/login")
        })
    }
}